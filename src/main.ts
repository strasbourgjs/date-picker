import '@/assets/css/style.css'
import { DatePickerInput } from '@/components/DatePickerInput'
import { DatePicker } from '@/components/DatePicker'
import type { Dayjs } from 'dayjs'
import dayjs from 'dayjs'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import 'dayjs/locale/fr'

dayjs.locale('fr')
dayjs.extend(localizedFormat)

customElements.define('date-picker-input', DatePickerInput)
customElements.define('date-picker', DatePicker)

let dateSelected: Dayjs = dayjs()

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
<date-picker-input model-value="${dateSelected.format('YYYY-MM-DD')}" label="test"></date-picker-input>
`

const datePicker = document.querySelector<DatePicker>('date-picker')

datePicker?.addEventListener('date-selected', (event) => {
  dateSelected = (event as CustomEvent<Dayjs>).detail
  document.querySelector<DatePickerInput>('date-picker-input')!
    .setAttribute('model-value', dateSelected.format('YYYY-MM-DD'))
  datePicker?.setAttribute('model-value', dateSelected.format('YYYY-MM-DD'))
})
