import datePickerInputStyle from '@/assets/css/datePickerInput.css?inline'
import type { Dayjs } from 'dayjs'

export class DatePickerInput extends HTMLElement {
  private _shadowRoot: ShadowRoot
  private _input: HTMLInputElement
  private _icon: SVGElement

  constructor() {
    super()
    this._shadowRoot = this.attachShadow({ mode: 'open' })

    this._shadowRoot.innerHTML = `
      <style>
      ${datePickerInputStyle}
      </style>
      <label class="date-picker_input">
      <input type="text"/>
      <span>test</span>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M19,19H5V8H19M16,1V3H8V1H6V3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3H18V1" />
      </svg>
      </label>
    `

    this._input = this._shadowRoot.querySelector('input')!
    this._icon = this._shadowRoot.querySelector('svg')!
  }

  static get observedAttributes() {
    return ['model-value', 'label']
  }

  connectedCallback() {
    this._input.addEventListener('input', () => {
      this.inputEvent()
    })

    this._input.value = this.getAttribute('model-value') || ''
    this.inputEvent()

    this._icon.addEventListener('click', () => {
      this.iconEvent()
    })
  }

  disconnectedCallback() {
    this._input.removeEventListener('input', () => {
      this.inputEvent()
    })
    this._icon.removeEventListener('click', () => {
      this.iconEvent()
    })
  }

  attributeChangedCallback(name: string, _oldValue: string, newValue: string) {
    if (name === 'model-value') {
      this._input.value = newValue
      this.inputEvent()
    }

    if (name === 'label') {
      this._shadowRoot.querySelector('label span')!.textContent = newValue
    }
  }

  private inputEvent() {
    if (!this._input.parentElement) return

    if (this._input.value && !this._input.parentElement.classList.contains('not-empty')) {
      this._input.parentElement.classList.add('not-empty')
    } else if (!this._input.value && this._input.parentElement.classList.contains('not-empty')) {
      this._input.parentElement.classList.remove('not-empty')
    }
  }

  private iconEvent() {
    const body = document.querySelector('body')!

    const datePickerContainer = document.createElement('div')
    datePickerContainer.classList.add('date-picker_container')
    datePickerContainer.addEventListener('click', (event) => {
      event.stopPropagation()
      body.removeChild(datePickerContainer)
    })

    const datePicker = document.createElement('date-picker')
    datePicker.setAttribute('model-value', this._input.value)
    datePicker.addEventListener('date-selected', (event) => {
      this._input.value = (event as CustomEvent<Dayjs>).detail.format('YYYY-MM-DD')
      body.removeChild(datePickerContainer)
    })

    datePickerContainer.appendChild(datePicker)

    body.appendChild(datePickerContainer)
  }
}
