import datePickerStyle from '@/assets/css/datePicker.css?inline'
import type { Dayjs } from 'dayjs'
import dayjs from 'dayjs'

export class DatePicker extends HTMLElement {
  private _shadowRoot: ShadowRoot
  private _container: HTMLDivElement
  private _dateDisplayed: Dayjs
  private _dateSelected: Dayjs

  private _yearHeaderButton: HTMLButtonElement
  private _yearHeader: HTMLButtonElement

  private readonly _body: HTMLElement
  private _dayBodyHeader: HTMLElement
  private _calendar: HTMLDivElement

  private _monthReadContainer: HTMLButtonElement
  private _lastMonthButton: HTMLButtonElement
  private _nextMonthButton: HTMLButtonElement

  private _yearReadContainer: HTMLButtonElement
  private _lastYearButton: HTMLButtonElement
  private _nextYearButton: HTMLButtonElement

  constructor() {
    super()
    this._shadowRoot = this.attachShadow({ mode: 'open' })

    this._dateSelected = this._dateDisplayed = this.getAttribute('model-value')
      ? dayjs(this.getAttribute('model-value') as string)
      : dayjs()

    this._shadowRoot.innerHTML = `
      <style>
      ${datePickerStyle}
      </style>
      
      <div class="date-picker">
      </div>
    `

    this._container = this._shadowRoot.querySelector('div.date-picker')!
    this._container.addEventListener('click', (event) => {
      event.stopPropagation()
    })

    this._yearHeaderButton = document.createElement('button')
    this._yearHeader = document.createElement('button')

    this._body = document.createElement('section')
    this._dayBodyHeader = document.createElement('header')
    this._calendar = document.createElement('div')

    this._monthReadContainer = document.createElement('button')
    this._lastMonthButton = document.createElement('button')
    this._nextMonthButton = document.createElement('button')

    this._yearReadContainer = document.createElement('button')
    this._lastYearButton = document.createElement('button')
    this._nextYearButton = document.createElement('button')

    this.generateCalendar()
  }

  static get observedAttributes() {
    return ['model-value']
  }

  attributeChangedCallback(name: string, _oldValue: string, newValue: string) {
    if (name === 'model-value' && newValue) {
      this._dateSelected = dayjs(newValue)
      this._dateDisplayed = dayjs(newValue)
      this.updateContainers()
    }
  }

  disconnectedCallback() {
    this._lastMonthButton.removeEventListener('click', () => this.lastMonthEvent())
    this._nextMonthButton.removeEventListener('click', () => this.nextMonthEvent())
    this._lastYearButton.removeEventListener('click', () => this.lastYearEvent())
    this._nextYearButton.removeEventListener('click', () => this.nextYearEvent())
  }

  private generateCalendar() {
    this.generateHeader()

    this.generateDaySection()
  }

  /**
   * Generate the header of the calendar.
   * The header contains the year and the date.
   * The year is a button to change the view in the section, to change the year.
   *
   * @private
   */
  private generateHeader(): void {
    const header = document.createElement('header')

    this._yearHeaderButton = document.createElement('button')
    this._yearHeaderButton.innerText = this._dateSelected.year().toString()
    this._yearHeaderButton.addEventListener('click', () => {
      this.generateYearSection()
    })
    header.appendChild(this._yearHeaderButton)

    this._yearHeader = document.createElement('button')
    this._yearHeader.innerText = this._dateSelected.format('ddd, DD MMMM')
    this._yearHeader.addEventListener('click', () => {
      this.generateDaySection()
    })
    header.appendChild(this._yearHeader)

    this._container.appendChild(header)
  }

  private generateDaySection(): void {
    this._body.innerHTML = ''
    this._body.className = ''

    this.generateHeaderDaySection()
    this.generateContentDaySection()
    this._body.classList.add('day-view')
    this._container.appendChild(this._body)
  }

  /**
   * Generate the header of the section.
   * The header contains the month and the year.
   * The month and the year are buttons to change the view in the section.
   * The month is a button to change the view in the section, to change the month.
   * The year is a button to change the view in the section, to change the year.
   * Add the header in the picker body.
   *
   * @private
   */
  private generateHeaderDaySection(): void {
    this._dayBodyHeader = document.createElement('header')

    const month = document.createElement('div')
    this._lastMonthButton = document.createElement('button')
    this._lastMonthButton.innerText = '<'
    this._lastMonthButton.classList.add('btn-icon')
    month.appendChild(this._lastMonthButton)
    this._monthReadContainer = document.createElement('button')
    this._monthReadContainer.innerText = this._dateDisplayed.format('MMMM')
    this._monthReadContainer.addEventListener('click', () => {
      this.generateMonthSection()
    })
    month.appendChild(this._monthReadContainer)
    this._nextMonthButton = document.createElement('button')
    this._nextMonthButton.innerText = '>'
    this._nextMonthButton.classList.add('btn-icon')
    month.appendChild(this._nextMonthButton)

    this._lastMonthButton.addEventListener('click', () => this.lastMonthEvent())
    this._nextMonthButton.addEventListener('click', () => this.nextMonthEvent())
    this._dayBodyHeader.appendChild(month)

    const year = document.createElement('div')
    this._lastYearButton = document.createElement('button')
    this._lastYearButton.innerText = '<'
    this._lastYearButton.classList.add('btn-icon')
    year.appendChild(this._lastYearButton)
    this._yearReadContainer = document.createElement('button')
    this._yearReadContainer.innerText = this._dateDisplayed.year().toString()
    this._yearReadContainer.addEventListener('click', () => {
      this.generateYearSection()
    })
    year.appendChild(this._yearReadContainer)
    this._nextYearButton = document.createElement('button')
    this._nextYearButton.innerText = '>'
    this._nextYearButton.classList.add('btn-icon')
    year.appendChild(this._nextYearButton)

    this._lastYearButton.addEventListener('click', () => this.lastYearEvent())
    this._nextYearButton.addEventListener('click', () => this.nextYearEvent())
    this._dayBodyHeader.appendChild(year)

    this._body.appendChild(this._dayBodyHeader)
  }

  private dateEvent(date: Dayjs): void {
    this.dispatchEvent(new CustomEvent('date-selected', { detail: date }))
  }

  /**
   * Generate the section of the calendar for the days.
   * The section contains the days of the month.
   * Add the section in the picker body.
   *
   * @private
   */
  private generateContentDaySection(): void {
    this._calendar = document.createElement('div');
    ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'].forEach(day => {
      const span = document.createElement('span')
      span.classList.add('day-head')
      span.innerText = day
      this._calendar.appendChild(span)
    })

    const firstDayOfMonth = this._dateDisplayed.startOf('month').day()

    for (let i = 1; i < (firstDayOfMonth || 7); i++) {
      const span = document.createElement('span')
      this._calendar.appendChild(span)
    }

    let day = dayjs(this._dateDisplayed).startOf('month')
    for (let i = 1; i <= this._dateDisplayed.daysInMonth(); i++) {
      const dateButton = document.createElement('button')
      dateButton.classList.add('btn-icon')
      dateButton.innerText = i.toString()
      if (day.isSame(this._dateSelected, 'day')) {
        dateButton.classList.add('selected')
      }

      dateButton.addEventListener('click', () => {
        this.dateEvent(dayjs(this._dateDisplayed).set('date', parseInt(dateButton.innerText as string)))
      })
      this._calendar.appendChild(dateButton)
      day = day.add(1, 'day')
    }

    this._body.appendChild(this._calendar)
  }

  /**
   * Generate the section of the calendar for the months.
   * The section contains the months of the year.
   * Add the section in the picker body.
   *
   * @private
   */
  private generateMonthSection(): void {
    this._body.innerHTML = ''
    this._body.className = '';

    ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'].forEach((month, index) => {
      const dateButton = document.createElement('button')
      dateButton.innerText = month
      dateButton.addEventListener('click', () => {
        this._dateDisplayed = this._dateDisplayed.set('month', index)
        this.generateDaySection()
      })
      this._body.appendChild(dateButton)
    })

    this._body.classList.add('month-view')
  }

  /**
   * Generate the section of the calendar for the years.
   * The section contains a years list.
   * At the left of the list, there is a button to go to the previous years list.
   * At the right of the list, there is a button to go to the next years list.
   * The years list contains the years of the decade.
   * Add the section in the picker body.
   *
   * @private
   */
  private generateYearSection(): void {
    this._body.innerHTML = ''
    this._body.className = ''

    const lastYears = document.createElement('button')
    lastYears.innerText = '<'
    lastYears.classList.add('btn-icon')
    lastYears.addEventListener('click', () => {
      this._dateDisplayed = this._dateDisplayed.subtract(10, 'year')
      this.generateYearSection()
    })
    this._body.appendChild(lastYears)

    this._calendar = document.createElement('div')
    const year = this._dateDisplayed.year() - (this._dateDisplayed.year() % 10)
    for (let i = 0; i < 10; i++) {
      const dateButton = document.createElement('button')
      dateButton.innerText = (year + i).toString()
      dateButton.addEventListener('click', () => {
        this._dateDisplayed = this._dateDisplayed.set('year', year + i)
        this.generateDaySection()
      })

      this._calendar.appendChild(dateButton)
    }

    this._body.appendChild(this._calendar)

    const nextYears = document.createElement('button')
    nextYears.innerText = '>'
    nextYears.classList.add('btn-icon')
    nextYears.addEventListener('click', () => {
      this._dateDisplayed = this._dateDisplayed.add(10, 'year')
      this.generateYearSection()
    })
    this._body.appendChild(nextYears)

    this._body.classList.add('year-view')
  }

  /**
   * Update the containers of the calendar.
   * The containers are the month, the year and the year in the header.
   * Remove the calendar section and regenerate it.
   *
   * @private
   */
  private updateContainers(): void {
    this._yearHeaderButton.innerText = this._dateSelected.year().toString()
    this._yearHeader.innerText = this._dateSelected.format('ddd, DD MMMM')

    this._monthReadContainer.innerText = this._dateDisplayed.format('MMMM')
    this._yearReadContainer.innerText = this._dateDisplayed.year().toString()

    this._body.removeChild(this._calendar)

    this.generateContentDaySection()
  }

  private lastMonthEvent(): void {
    this._dateDisplayed = this._dateDisplayed.subtract(1, 'month')
    this.updateContainers()
  }

  private nextMonthEvent(): void {
    this._dateDisplayed = this._dateDisplayed.add(1, 'month')
    this.updateContainers()
  }

  private lastYearEvent(): void {
    this._dateDisplayed = this._dateDisplayed.subtract(1, 'year')
    this.updateContainers()
  }

  private nextYearEvent(): void {
    this._dateDisplayed = this._dateDisplayed.add(1, 'year')
    this.updateContainers()
  }
}
